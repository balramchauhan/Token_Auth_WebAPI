﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using System.Threading.Tasks;
using TokenAuth.Models;

namespace TokenAuth
{
    public class MyAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            TokenAuthDBEntities myentity = new TokenAuthDBEntities();
            string tempPassword = Crypto.Hash(context.Password);
            var uservalue = myentity.UsersTables.Where(u => u.UserName == context.UserName && u.Password == tempPassword).FirstOrDefault();
            if(uservalue!=null)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role,uservalue.Role));
                identity.AddClaim(new Claim("username", uservalue.UserName));
                identity.AddClaim(new Claim(ClaimTypes.Name, uservalue.Name));
                context.Validated(identity);
            }
            else
            {
                context.SetError("invalid_grant", "Provided username and password is incorrect");
                return;
            }
        }
    }
}