﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TokenAuth.Models;

namespace TokenAuth.Controllers
{
    public class AccountController : ApiController
    {
        [AllowAnonymous]
        [HttpPost]
        [Route("api/account/register")]
        public IHttpActionResult Post(UsersTable obj)
        {
            TokenAuthDBEntities myentitty = new TokenAuthDBEntities();
            var status = myentitty.UsersTables.Where(u => u.UserName == obj.UserName).FirstOrDefault();
            if (status == null)
            {
                obj.Password = Crypto.Hash(obj.Password);
                myentitty.UsersTables.Add(obj);
                myentitty.SaveChanges();
                return Ok("Great! Account created now you can login.");
            }
            else
            {
                return Ok("Sorry! UserName already registered, please try different.");
            }
        }
    }
}
